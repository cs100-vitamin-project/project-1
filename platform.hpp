#ifndef PLATFORM_HPP_
#define PLATFORM_HPP_

#include <iostream>
#include <iomanip>
#include <cstdlib>

typedef int (*chess_type)[4];

class Game
{
public:
    
    /* ************** TASK 1  FUNCTION PROTOTYPE ****************** *
    * Welcome to C plus plus (C++) !                                *
    *                                                               *
    * Your task is to read functions in platform.cpp carefully and  *
    * find the missing prototypes below.                            *
    *                                                               *
    * There are 10 lines in total! Remember that BasicMove should   *
    * not appear here because the prototype is already in line 36.  *
    *                                                               *
    * When you finish, type                                         *
    *                                                               *
    *     g++ 2048_keyboard.cpp platform.cpp -o 2048                *
    *                                                               *
    * to compile, and get '2048' or '2048.exe'.                     *
    * Then enjoy the game!                                          *
    * ************************************************************* */

    // YOUR CODE HERE

    void print();

private:
    int BasicMove(int line[]);

    int chess[4][4] = {};
    int score;
};

#endif

#ifdef _WIN32

#include <windows.h>

#endif
