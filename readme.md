# Project 1 - C++ Class Function Prototype

Welcome to C plus plus (C++) ! 

The whole project is based on game 2048. If you are not familiar with this popular game, please refer to [this website](https://play2048.co//).

The first project will not be very hard (just a warm up), but hopefully you can enjoy youself.

---
## Files

There is 1 task in total.

Files to modify:

    platform.hpp

Files to read:

    platform.cpp

Files that should not be modified:

    2048_keyboard.cpp

---
## Task 1

Welcome to C plus plus (C++) !

Your task is to read functions in `platform.cpp` carefully and find the missing prototypes in `platform.hpp`.

There are 10 lines in total! Remember that `BasicMove` should not appear here because the prototype is already in line 36, `platform.hpp`. No distructor, since I didn't store anything in heap.

When you finish, type

    g++ 2048_keyboard.cpp platform.cpp -o 2048

in the console to compile, and get `2048` or `2048.exe`. Run it and enjoy the game (play with arrow keys)!