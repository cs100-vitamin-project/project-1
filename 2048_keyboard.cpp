#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <ctime>
#include "platform.hpp"

#ifdef _WIN32

#include <conio.h>
#include <windows.h>
#define KEY_CODE_UP 72
#define KEY_CODE_DOWN 80
#define KEY_CODE_LEFT 75
#define KEY_CODE_RIGHT 77

#else

#include <unistd.h>
#include <signal.h>
#include <termios.h>
#include <string.h>
#include <fcntl.h>
#define Sleep(s) sleep(s)
#define KEY_CODE_UP 0x41
#define KEY_CODE_DOWN 0x42
#define KEY_CODE_LEFT 0x44
#define KEY_CODE_RIGHT 0x43

struct termios cooked, raw;

void quit(int sig)
{
  (void)sig;
  tcsetattr(0, TCSANOW, &cooked); // re config at the end
  exit(0);
}

#endif


void init_game();
int read_keyboard();
void keyboard_play(Game &game);

int main(int argc, char const *argv[])
{
    init_game();
    
    system("mode con cols=40 lines=15");

    /* ******* 3 lines to play the game! ******* */
    Game game;             // get an object
    game.print();          // print it out
    keyboard_play(game);   // play it!
    /* ***************************************** */

#ifdef _WIN32
    system("pause");
#endif

    return 0;
}



void init_game()
{
    srand(int(time(0)));

#ifndef _WIN32
    signal(SIGINT,quit);

    tcgetattr(0, &cooked);
    memcpy(&raw, &cooked, sizeof(struct termios));
    raw.c_lflag &=~ (ICANON | ECHO);
    raw.c_cc[VEOL] = 1;
    raw.c_cc[VEOF] = 2;
    tcsetattr(0, TCSANOW, &raw);
#endif
}

int read_keyboard()
{
#ifdef _WIN32
    return _getch();
#else
    int key_code;
    if (read(0, &key_code, 1) < 0)
    {
        return -1;
    }
    return key_code;
#endif
}

void keyboard_play(Game &game)
{
    while (true)
    {
        
        int cmd = read_keyboard();
        if (game.isPossibleArrow('W') && cmd == KEY_CODE_UP)
            game.Move('W');
        else if (game.isPossibleArrow('S') && cmd == KEY_CODE_DOWN)
            game.Move('S');
        else if (game.isPossibleArrow('A') && cmd == KEY_CODE_LEFT)
            game.Move('A');
        else if (game.isPossibleArrow('D') && cmd == KEY_CODE_RIGHT)
            game.Move('D');
        else
        {
            continue;
        }
        
        game.RandomAdd();
        game.print();
        if (game.isDead())
        {
            std::cout << "GAME OVER!" << std::endl;
            break;
        }
    }
}
